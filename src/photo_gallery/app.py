from pathlib import Path
import os
import uuid

from bottle import jinja2_view
from bottle import get
from bottle import post
from bottle import run
from bottle import static_file
from bottle import request
from bottle import redirect
from bottle import abort

from .config import COLUMN_COUNT
from .config import DATABASE_FILE
from .config import PHOTOS_DIR
from .config import PHOTOS_URL
from .utils import get_database_connection
from .utils import get_photos_count
from .utils import get_photos_info
from .utils import get_rows
from .utils import add_photo_info


@get('/')
@jinja2_view('index.html')
def photos():
    """Страница с фотографиями галлереи."""
    with get_database_connection(DATABASE_FILE) as dbc:
        photos_count = get_photos_count(dbc)
        photos_info = get_photos_info(dbc)

        rows = get_rows(photos_info, photos_count, COLUMN_COUNT)

        return dict(
            PHOTOS_URL=PHOTOS_URL,
            rows=rows,
        )


@get('/upload')
@jinja2_view('upload.html')
def upload():
    """Страница загрузки новой фотографии."""
    return dict()


@post('/upload')
def do_upload():
    """Сохранение фотографии."""
    with get_database_connection(DATABASE_FILE) as dbc:
        photo = request.files.get('photo')
        title = request.forms.get('title')

        if photo:
            name, ext = os.path.splitext(photo.filename)
            if ext not in ('.jpg', '.png', '.jpeg'):
                abort(406, 'File extension not allowed')
            photo.filename = ''.join((str(uuid.uuid4()), ext))
            photo.save(PHOTOS_DIR)
            add_photo_info(dbc, photo.filename, title)

    redirect('/')


@get('/photos/<filename>')
def get_file(filename):
    """Файл фотографии."""
    return static_file(filename, root=PHOTOS_DIR)

if __name__ == '__main__':
    from bottle import TEMPLATE_PATH

    TEMPLATE_PATH[:] = [str(Path(__file__).parent / 'templates')]

    run(host='localhost', port=8080)
